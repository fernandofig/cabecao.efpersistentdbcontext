@echo off

if "%1" == "Debug" (
	echo Projeto em configuracao de Debug : Nao publicando pacote Nuget
	exit
)

cd %2

nuget pack %3.csproj -Prop Configuration=Release
forfiles /P %2 /m *.nupkg /c "cmd /c NuGet.exe push @FILE -Source http://nuget.fuah.com.br/api/v2/"
del /q *.nupkg