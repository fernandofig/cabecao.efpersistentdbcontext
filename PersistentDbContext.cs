﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Web;

namespace System.Data.Entity {
	public partial class PersistentDbContext : DbContext {
		private static Dictionary<Type, PersistType> _containerStore = new Dictionary<Type, PersistType>();

		private static Dictionary<Type, PersistentDbContext> _persistentContext = new Dictionary<Type, PersistentDbContext>();

		public bool IsDisposed { get; private set; }

		protected PersistentDbContext(string connString) : base(connString) { IsDisposed = false; }

		protected static void AddContainer<T>(PersistType persistType) {
			_containerStore[typeof(T)] = persistType;
		}

		protected static T GetContext<T>(string connString) where T : PersistentDbContext {
			T _ctx;

			PersistType pt = GetPersistType(typeof(T));
			string contextId = "EFAR_Context_" + (typeof(T)).Name;
			BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;

			switch (pt) {
			case PersistType.None:
				_ctx = (T)Activator.CreateInstance(typeof(T), flags, null, new object[] { connString }, null);
				break;
			case PersistType.Manual:
				if (!_persistentContext.ContainsKey(typeof(T)) || _persistentContext[typeof(T)].IsDisposed)
					_persistentContext[typeof(T)] = null;

				if (_persistentContext[typeof(T)] != null) {
					_ctx = (T)_persistentContext[typeof(T)];
				} else {
					_ctx = (T)Activator.CreateInstance(typeof(T), flags, null, new object[] { connString }, null);
					_persistentContext[typeof(T)] = _ctx;
				}
				break;
			default:
				if (HttpContext.Current.Items[contextId] != null)
					_ctx = (T)HttpContext.Current.Items[contextId];
				else {
					_ctx = (T)Activator.CreateInstance(typeof(T), flags, null, new object[] { connString }, null);
					HttpContext.Current.Items.Add(contextId, _ctx);
				}
				break;
			}

			return _ctx;
		}

		protected static void DisposeContext<T>() where T : PersistentDbContext {
			PersistType pt = GetPersistType(typeof(T));
			string contextId = "EFAR_Context_" + (typeof(T)).Name;

			switch (pt) {
			case PersistType.Manual:
				if (!_persistentContext.ContainsKey(typeof(T)))
					_persistentContext[typeof(T)] = null;

				if (_persistentContext[typeof(T)] != null) {
					_persistentContext[typeof(T)].Dispose(true);
					_persistentContext[typeof(T)] = null;
				}
				break;
			default:
				if (HttpContext.Current.Items[contextId] != null) {
					T _ctx = (T)HttpContext.Current.Items[contextId];
					_ctx.Dispose();
					HttpContext.Current.Items.Remove(contextId);
				}
				break;
			}
		}

		public EntityKey GetEntityKey<T>(T entity) where T : class {
			var oc = ((IObjectContextAdapter)this).ObjectContext;
			ObjectStateEntry ose;
			if (null != entity && oc.ObjectStateManager.TryGetObjectStateEntry(entity, out ose))
				return ose.EntityKey;
			
			return null;
		}

		public EntityKey GetEntityKey<T>(DbEntityEntry<T> dbEntityEntry) where T : class {
			if (dbEntityEntry != null)
				return GetEntityKey(dbEntityEntry.Entity);
			
			return null;
		}

		private static PersistType GetPersistType(Type container) {
			if (!_containerStore.ContainsKey(container)) throw new Exception("Container com o Tipo de Objeto especificado não foi encontrado no store");

			return _containerStore[container];
		}

		protected override void Dispose(bool disposing) {
			base.Dispose(disposing);

			IsDisposed = true;
		}
	}

	public enum PersistType {
		WebRequest = 0,
		Manual = 1,
		None = 2
	}
}
