﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Data.Entity;

using YOURAPP.Models;

namespace YOURAPP {
	public class MvcApplication : System.Web.HttpApplication {
		protected void Application_Start() {
			AreaRegistration.RegisterAllAreas();
			RouteConfig.RegisterRoutes(RouteTable.Routes);

			YOURAPPDataContainer.AddContainer(PersistType.WebRequest);
		}

		protected void Application_BeginRequest(object sender, EventArgs e) {
			YOURAPPDataContainer.GetContext();
		}

		protected void Application_EndRequest(object sender, EventArgs e) {
			YOURAPPDataContainer.DisposeContext();
		}
	}
}
